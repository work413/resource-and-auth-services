# Picture It

This system is created consisting of two services (Auth and Resource) that are developed here and one already existing service (Image).

From the consumer's perspective, the API has a single entry point even though it consists of multiple back-end services.

The system handles images (called "resources") in a Restful manner through the use of three "microservices."

### System overview

![Overall overview of the architecture](.readme/overall_architecture.png)

_The image service is already deployed._

### Flow chart

![Flow chart of the application](.readme/flow_chart.png)

0) The client tries to contact the resource service but gets a 403 back.
1) The client registers an account and logs in to the system. It will receive a JWT upon successful login.
2) Using the JWT as a Bearer token, the client can add, update and delete resources in the system.
3) When the client does a C- U- or D-request, the Resource service will send a request to the Image-service to add, delete or update an image. The Resource service stores the resources together with image "metadata." The Resource service responds to the client with the created document.
4) The client can request the image from the URL provided by the resource service.

## Auth service

The Auth service is a service that authenticates users and hands out JWT on login. The JWT handed out by the Auth-service are validated and trusted by the Resource-service to use account information stored in the JWT without contacting the Auth-service.

## Resource service

This service handles the resources in the overall application. In this case, this includes things like the user's image-URLs, image-descriptions, and image-title. The images will not be stored in this service, though. Instead, an external image service will be used to store the images. 

## Image service (existing)

This service will store all images that the application needs. This service is already deployed, and you find it at:
`https://courselab.lnu.se/picture-it/images/api/v1/`

In [its documenation](https://courselab.lnu.se/picture-it/images/api/v1/doc/) you will find out how to communicate with the service. Some things to note:

- Image data needs to be sent as a Base64-encoded string.
- The service communicates using an Access token. You will find the token in your "Secrets"-project.
- The service has a public interface on which it serves the images. Only the image URL is needed to be stored in the resource service.
- The payload to send to the server can not exceed 500kb. Yes, this means that only small images can be handled!

