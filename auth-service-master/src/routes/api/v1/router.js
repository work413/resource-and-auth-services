/**
 * API version 1 routes.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import { router as accountRouter } from './account-router.js'

export const router = express.Router()

router.get('/', (req, res) => res.json({ message: 'Welcome to the API.' }))
router.use('/', accountRouter)
