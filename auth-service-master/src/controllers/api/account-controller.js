/**
 * Module for the Account Controller.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import jwt from 'jsonwebtoken'
import fs from 'fs'
import createError from 'http-errors'
import { User } from '../../models/user.js'

/**
 * The account controller.
 */
export class AccountController {
  /**
   * Logs in a user.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async login (req, res, next) {
    try {
      const user = await User.authenticate(req.body.email, req.body.password)

      const payload = {
        sub: user.email,
        x_permission_level: user.permissionLevel
      }

      // Read the private key.
      const privateKey = fs.readFileSync('./private.pem', 'utf8')

      // Create the access token.
      const accessToken = jwt.sign(payload, privateKey, {
        algorithm: 'RS256',
        expiresIn: '1h'
      })

      const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET)

      res
        .status(201)
        .json({
          access_token: accessToken,
          refresh_token: refreshToken
        })
    } catch (error) {
      const err = createError(401)
      err.message = 'Invalid username or password'

      next(err)
    }
  }

  /**
   * Registers a user.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async register (req, res, next) {
    try {
      // Creates user object from model.
      const user = await User.insert({
        email: req.body.email,
        password: req.body.password,
        permissionLevel: 8
      })

      res.status(201).json({ id: user.id })
    } catch (error) {
      let err = error

      if (err.code === 11000) {
        // Duplicated keys.
        err = createError(409)
        err.message = 'Duplicated keys'
      } else if (error.name === 'ValidationError') {
        // Validation error(s).
        err = createError(400)
        err.message = 'Validation Error'
      }

      next(err)
    }
  }
}
