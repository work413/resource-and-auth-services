/**
 * The starting point of the web service.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import helmet from 'helmet'
import logger from 'morgan'
import { connectDB } from './config/mongoose.js'
import { router } from './routes/router.js'

/**
 * The main function of the web service.
 */
const main = async () => {
  await connectDB()

  const app = express()

  app.use(helmet())
  app.use(logger('dev'))

  app.use(express.json({ limit: '500kb' }))

  app.use('/', router)

  // Handles errors.
  app.use(function (err, req, res, next) {
    err.status = err.status || 500

    if (req.app.get('env') !== 'development') {
      res
        .status(err.status)
        .json({
          status: err.status,
          message: err.message
        })
      return
    }

    return res.status(err.status).json(err)
  })

  // Starts the server.
  app.listen(process.env.PORT, () => {
    console.log(`Server running at http://localhost:${process.env.PORT}`)
    console.log('Press Ctrl-C to terminate...')
  })
}

main().catch(console.error)
