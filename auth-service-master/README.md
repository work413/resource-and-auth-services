# Auth service

The Auth-service is responsible for handling accounts and handing out JWTs upon successful authentication. 

## Generate RS256 key pairs

The JWTs issued by the API should use RS256 encryption (asymmetric). To achieve this, you need to generate private and public keys. 

```bash
$ openssl genrsa -out private.pem 2048
Generating RSA private key, 2048 bit long modulus
.....+++++
...................................+++++
e is 65537 (0x010001)

$ openssl rsa -in private.pem -pubout -out public.pem
writing RSA key

```

## Multiple MongoDB instances

It is recommended that each service runs its own MongoDB-instance on the production server even though it would be possible to use the same one or an existing one on the production server. The reason for this is that services in microservice architectures should strive to be loosely coupled so that they can be developed, deployed, and scaled independently. 

