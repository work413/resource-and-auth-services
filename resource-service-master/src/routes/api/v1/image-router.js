/**
 * API version 1 image routes.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import createError from 'http-errors'
import { ImageController } from '../../../controllers/api/image-controller.js'

export const router = express.Router()

const controller = new ImageController()

/**
 * Authenticates requests.
 *
 * If authentication is successful, `req.user`is populated and the
 * request is authorized to continue.
 * If authentication fails, an unauthorized response will be sent.
 *
 * @param {object} req - Express request object.
 * @param {object} res - Express response object.
 * @param {Function} next - Express next middleware function.
 */
const authenticate = (req, res, next) => {
  const authorization = req.headers.authorization?.split(' ')

  if (authorization?.[0] !== 'Bearer') {
    const err = createError(401)
    err.message = 'Bearer token is missing'
    next(err)
    return
  }

  // The public key.
  const publicKey = fs.readFileSync('./public.pem', 'utf8')

  try {
    // Verifies the jwt against the public key.
    const payload = jwt.verify(authorization[1], publicKey)
    req.user = {
      email: payload.sub
    }

    next()
  } catch (error) {
    const err = createError(403)
    err.message = 'JWT Validation failed'
    next(err)
  }
}

router.param('id', (req, res, next, id) => controller.loadImage(req, res, next, id))

router.get('/', authenticate, (req, res, next) => controller.findAll(req, res, next))

router.post('/', authenticate, (req, res, next) => controller.create(req, res, next))

router.get('/:id', authenticate, (req, res, next) => controller.find(req, res, next))

router.put('/:id', authenticate, (req, res, next) => controller.update(req, res, next))

router.patch('/:id', authenticate, (req, res, next) => controller.modify(req, res, next))

router.delete('/:id', authenticate, (req, res, next) => controller.delete(req, res, next))
