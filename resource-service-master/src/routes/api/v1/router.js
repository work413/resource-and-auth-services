/**
 * API version 1 routes.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import express from 'express'
import { router as imageRouter } from './image-router.js'

export const router = express.Router()

router.use('/images', imageRouter)
