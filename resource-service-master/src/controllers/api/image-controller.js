/**
 * Module for the ImageController.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import createError from 'http-errors'
import { ImagePost } from '../../models/image-post.js'
import { Image } from '../../models/image.js'
import { ImagePatch } from '../../models/image-patch.js'
import fetch from 'node-fetch'

/**
 * Encapsulates a controller.
 */
export class ImageController {
  /**
   * Provide req.img to the route if :id is present.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   * @param {string} id - The value of the id for the image to load.
   */
  async loadImage (req, res, next, id) {
    try {
      const img = await Image.getById(id)

      // If no image found send a 404 (Not Found).
      if (img === 'undefined' || img === null) {
        const err = createError(404)
        err.message = 'Image with id not found'
        next(err)
        return
      }

      req.img = img

      // Next middleware.
      next()
    } catch (error) {
      next(error)
    }
  }

  /**
   * Sends a JSON response containing an image.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async find (req, res, next) {
    res.json(req.img)
  }

  /**
   * Sends a JSON response containing all images.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async findAll (req, res, next) {
    try {
      const images = await Image.getAll()
      res.json(images)
    } catch (error) {
      next(error)
    }
  }

  /**
   * Updates a specific image.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async update (req, res, next) {
    try {
      // Creates a new image with the new data.
      const updated = await req.img.update(req.body)
      // Send a put request to the image service.
      await fetch('https://courselab.lnu.se/picture-it/images/api/v1/images/' + req.img.id, {
        method: 'PUT',
        headers: {
          'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          data: req.body.data,
          contentType: req.body.contentType
        })
      })

      // Sets the image data in the order demonstrated in the documentation.
      const ordered = {
        imageUrl: updated.imageUrl,
        location: updated.location,
        description: updated.description,
        createdAt: updated.createdAt,
        updatedAt: updated.updatedAt,
        id: updated.id
      }

      res.send(ordered)
      res.status(204).end()
    } catch (error) {
      let err = error
      if (error.name === 'ValidationError') {
        err = createError(400)
        err.message = 'Validation Error'
      }
      next(err)
    }
  }

  /**
   * Modifies a specific image.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async modify (req, res, next) {
    try {
      // Modifies the description.
      const patchImg = await ImagePatch.modify(req.body)
      // Updates the image object with the new change.
      await req.img.update(patchImg)

      res.status(204).json({ message: 'Image partially updated' })
    } catch (error) {
      let err = error
      if (error.name === 'ValidationError') {
        err = createError(400)
        err.innerException = error
      }
      next(err)
    }
  }

  /**
   * Deletes the specified image.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async delete (req, res, next) {
    try {
      // Deletes the image.
      await req.img.delete()
      // Sends a delete request to the image service.
      await fetch('https://courselab.lnu.se/picture-it/images/api/v1/images/' + req.img.id, {
        method: 'DELETE',
        headers: {
          'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN
        }
      })
      res.status(204).json({ message: 'Image deleted' })
    } catch (error) {
      next(error)
    }
  }

  /**
   * Creates a new image.
   *
   * @param {object} req - Express request object.
   * @param {object} res - Express response object.
   * @param {Function} next - Express next middleware function.
   */
  async create (req, res, next) {
    try {
      // Creates a new image post object.
      const image = await ImagePost.insert(req.body)
      image.contentType = 'image/gif'
      // Sends a post request to the image service.
      const postImage = await fetch('https://courselab.lnu.se/picture-it/images/api/v1/images/', {
        method: 'POST',
        headers: {
          'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          data: image.data,
          contentType: 'image/gif'
        })
      }).then(response => {
        return response.json()
      })

      // Creates a new image.
      const img = await Image.insert({
        imageUrl: postImage.imageUrl,
        location: req.body.location,
        description: req.body.description,
        createdAt: postImage.createdAt,
        updatedAt: postImage.updatedAt,
        id: postImage.id
      }).then(response => {
        return response
      })

      // The location to send in response.
      const location = new URL(
        `${req.protocol}://${req.get('host')}${req.originalUrl}/${img.id}`
      )

      res
        .location(location.href)
        .status(201)
        .json(img)
    } catch (error) {
      let err = error
      err = createError(500)
      err.innerException = error
      next(error)
    }
  }
}
