/**
 * Mongoose model for image patch request.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import mongoose from 'mongoose'

// Create a schema.
const schema = new mongoose.Schema({
  description: {
    type: String
  },
  location: {
    type: String
  }
}, {
  timestamps: true,
  versionKey: false,
  toJSON: {
    /**
     * Performs a transformation of the resulting object to remove sensitive information.
     *
     * @param {object} doc - The mongoose document which is being converted.
     * @param {object} ret - The plain object representation which has been converted.
     */
    transform: function (doc, ret) {
      delete ret._id
    },
    virtuals: true // ensure virtual fields are serialized
  }
})

/**
 * Gets a image by ID.
 *
 * @param {string} id - The value of the id for the task to get.
 * @returns {Promise<Image>} The Promise to be fulfilled.
 */
schema.statics.getById = async function (id) {
  return this.findOne({ id: id })
}

/**
 * Modifies an image.
 *
 * @param {object} imageData - ...
 * @returns {Promise<Image>} The Promise to be fulfilled.
 */
schema.statics.modify = async function (imageData) {
  const image = new ImagePatch(imageData)
  image.location = imageData.location
  image.description = imageData.description
  return image.save()
}

// Create a model using the schema.
export const ImagePatch = mongoose.model('ImagePatch', schema)
