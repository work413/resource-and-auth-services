/**
 * Mongoose model for image.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import mongoose from 'mongoose'

// Create a schema.
const schema = new mongoose.Schema({
  imageUrl: {
    type: String,
    required: true
  },
  location: {
    type: String
  },
  description: {
    type: String
  },
  createdAt: {
    type: String
  },
  updatedAt: {
    type: String
  },
  id: {
    type: String
  }
}, {
  timestamps: true,
  versionKey: false,
  toJSON: {
    /**
     * Performs a transformation of the resulting object to remove sensitive information.
     *
     * @param {object} doc - The mongoose document which is being converted.
     * @param {object} ret - The plain object representation which has been converted.
     */
    transform: function (doc, ret) {
      delete ret._id
    },
    virtuals: true // ensure virtual fields are serialized
  }
})

/**
 * Gets all images.
 *
 * @returns {Promise<Image[]>} The Promise to be fulfilled.
 */
schema.statics.getAll = async function () {
  return this.find({})
}

/**
 * Gets a image by ID.
 *
 * @param {string} id - The value of the id for the task to get.
 * @returns {Promise<Image>} The Promise to be fulfilled.
 */
schema.statics.getById = async function (id) {
  return this.findOne({ id: id })
}

/**
 * Updates an image.
 *
 * @param {object} imgData - The image object's data.
 * @returns {Promise} The Promise to be fulfilled.
 */
schema.methods.update = async function (imgData) {
  if (imgData.location?.localeCompare(this.location) !== 0) {
    this.location = imgData.location
  }
  if (imgData.description?.localeCompare(this.description) !== 0) {
    this.description = imgData.description
  }
  return this.save()
}

/**
 * Inserts a new images.
 *
 * @param {object} imageData - ...
 * @param {string} imageData.description - ...
 * @param {boolean} imageData.done - ...
 * @returns {Promise<Image>} The Promise to be fulfilled.
 */
schema.statics.insert = async function (imageData) {
  const image = new Image(imageData)
  return image.save()
}

/**
 * Deletes an image.
 *
 * @returns {Promise} The Promise to be fulfilled.
 */
schema.methods.delete = async function () {
  return this.remove()
}

// Create a model using the schema.
export const Image = mongoose.model('Image', schema)
