/**
 * Mongoose model for image post request.
 *
 * @author Martina Andersson
 * @version 1.0.0
 */

import mongoose from 'mongoose'

// Create a schema.
const schema = new mongoose.Schema({
  data: {
    type: String,
    required: true
  },
  contentType: {
    type: String,
    required: true
  },
  location: {
    type: String
  },
  description: {
    type: String
  }
}, {
  timestamps: true,
  versionKey: false,
  toJSON: {
    /**
     * Performs a transformation of the resulting object to remove sensitive information.
     *
     * @param {object} doc - The mongoose document which is being converted.
     * @param {object} ret - The plain object representation which has been converted.
     */
    transform: function (doc, ret) {
      delete ret._id
    },
    virtuals: true // ensure virtual fields are serialized
  }
})

/**
 * Inserts a new images.
 *
 * @param {object} imageData - ...
 * @param {string} imageData.description - ...
 * @param {boolean} imageData.done - ...
 * @returns {Promise<Image>} The Promise to be fulfilled.
 */
schema.statics.insert = async function (imageData) {
  const image = new ImagePost(imageData)
  return image.save()
}

// Create a model using the schema.
export const ImagePost = mongoose.model('ImagePost', schema)
